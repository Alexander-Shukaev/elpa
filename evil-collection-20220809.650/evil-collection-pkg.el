(define-package "evil-collection" "20220809.650" "A set of keybindings for Evil mode"
  '((emacs "26.3")
    (evil "1.2.13")
    (annalist "1.0"))
  :commit "c1ede88de9b09f280e4a37c671b6dd540bf1028a" :authors
  '(("James Nguyen" . "james@jojojames.com"))
  :maintainer
  '("James Nguyen" . "james@jojojames.com")
  :keywords
  '("evil" "tools")
  :url "https://github.com/emacs-evil/evil-collection")
;; Local Variables:
;; no-byte-compile: t
;; End:
