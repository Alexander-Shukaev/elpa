;;; sync-recentf-autoloads.el --- automatically extracted autoloads
;;
;;; Code:

(add-to-list 'load-path (directory-file-name
                         (or (file-name-directory #$) (car load-path))))


;;;### (autoloads nil "sync-recentf" "sync-recentf.el" (0 0 0 0))
;;; Generated autoloads from sync-recentf.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "sync-recentf" '("recentf-save-list" "sync-recentf-")))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; sync-recentf-autoloads.el ends here
