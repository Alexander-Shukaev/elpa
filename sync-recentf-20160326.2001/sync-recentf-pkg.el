;;; -*- no-byte-compile: t -*-
(define-package "sync-recentf" "20160326.2001" "Synchronize the recent files list between Emacs instances" 'nil :commit "0052561d5c5b5c2684faedc3eead776aec06c3ed" :authors '(("François Févotte" . "fevotte@gmail.com")) :maintainer '("François Févotte" . "fevotte@gmail.com") :keywords '("recentf") :url "https://github.com/ffevotte/sync-recentf")
