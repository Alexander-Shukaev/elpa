;;; -*- no-byte-compile: t -*-
(define-package "historian" "20200203.1927" "Persistently store selected minibuffer candidates" '((emacs "24.4")) :commit "852cb4e72c0f78c8dbb2c972bdcb4e7b0108ff4c" :authors '(("PythonNut" . "pythonnut@pythonnut.com")) :maintainer '("PythonNut" . "pythonnut@pythonnut.com") :keywords '("convenience") :url "https://github.com/PythonNut/historian.el")
