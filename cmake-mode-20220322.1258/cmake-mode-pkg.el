;;; -*- no-byte-compile: t -*-
(define-package "cmake-mode" "20220322.1258" "major-mode for editing CMake sources" '((emacs "24.1")) :commit "33a847c84d68220c3b9483529d28062acdc334ff")
