(define-package "magit-annex" "20230407.1200" "Control git-annex from Magit"
  '((cl-lib "0.3")
    (magit "3.0.0"))
  :commit "255e443e19a32e716ff414e09ad5e00f6f8bc8fb" :authors
  '(("Kyle Meyer" . "kyle@kyleam.com")
    ("Rémi Vanicat" . "vanicat@debian.org"))
  :maintainers
  '(("Kyle Meyer" . "kyle@kyleam.com"))
  :maintainer
  '("Kyle Meyer" . "kyle@kyleam.com")
  :keywords
  '("vc" "tools")
  :url "https://github.com/magit/magit-annex")
;; Local Variables:
;; no-byte-compile: t
;; End:
