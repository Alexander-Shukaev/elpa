(define-package "electric-operator" "20220417.809" "Automatically add spaces around operators"
  '((dash "2.10.0")
    (emacs "24.4"))
  :commit "f567f03da4a55d6eafa0e6e148ca4884d5370498" :authors
  '(("David Shepherd" . "davidshepherd7@gmail.com"))
  :maintainer
  '("David Shepherd" . "davidshepherd7@gmail.com")
  :keywords
  '("electric")
  :url "https://github.com/davidshepherd7/electric-operator")
;; Local Variables:
;; no-byte-compile: t
;; End:
