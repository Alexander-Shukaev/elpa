;;; ob-compile-autoloads.el --- automatically extracted autoloads  -*- lexical-binding: t -*-
;;
;;; Code:

(add-to-list 'load-path (directory-file-name
                         (or (file-name-directory #$) (car load-path))))


;;;### (autoloads nil "ob-compile" "ob-compile.el" (0 0 0 0))
;;; Generated autoloads from ob-compile.el

(autoload 'org-babel-execute:compile "ob-compile" "\
Orgmode Babel COMPILE evaluate function for `BODY' with `PARAMS'.

\(fn BODY PARAMS)" nil nil)

(register-definition-prefixes "ob-compile" '("ob-compile" "org-babel-default-header-args:compile"))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; ob-compile-autoloads.el ends here
