;;; -*- no-byte-compile: t -*-
(define-package "ivy-hydra" "20220402.1348" "Additional key bindings for Ivy" '((emacs "24.5") (ivy "0.13.4") (hydra "0.14.0")) :commit "7489968257a74f176c0d1de7ec8bd1e2011f0db4" :authors '(("Oleh Krehel" . "ohwoeowho@gmail.com")) :maintainer '("Oleh Krehel" . "ohwoeowho@gmail.com") :keywords '("convenience") :url "https://github.com/abo-abo/swiper")
