(define-package "ac-php-core" "20220418.419" "The core library of the ac-php"
  '((dash "1")
    (php-mode "1")
    (s "1")
    (f "0.17.0")
    (popup "0.5.0")
    (xcscope "1.0"))
  :commit "f34e09783b77d1158ea139b7b3d8034bc52b0b9f" :authors
  '(("jim" . "xcwenn@qq.com")
    ("Serghei Iakovlev" . "sadhooklay@gmail.com"))
  :maintainer
  '("jim")
  :keywords
  '("completion" "convenience" "intellisense")
  :url "https://github.com/xcwen/ac-php")
;; Local Variables:
;; no-byte-compile: t
;; End:
