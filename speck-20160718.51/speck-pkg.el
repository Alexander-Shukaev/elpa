;;; -*- no-byte-compile: t -*-
(define-package "speck" "20160718.51" "minor mode for spell checking" 'nil :commit "0ee05e46913f5ddda40c34676ecb7ca0cff6a4fd" :keywords '("spell" "checking") :authors '(("Martin Rudalics" . "rudalics@gmx.at")) :maintainer '("Martin Rudalics" . "rudalics@gmx.at"))
