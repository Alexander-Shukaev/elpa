;;; ob-git-permalink-autoloads.el --- automatically extracted autoloads  -*- lexical-binding: t -*-
;;
;;; Code:

(add-to-list 'load-path (directory-file-name
                         (or (file-name-directory #$) (car load-path))))


;;;### (autoloads nil "ob-git-permalink" "ob-git-permalink.el" (0
;;;;;;  0 0 0))
;;; Generated autoloads from ob-git-permalink.el

(autoload 'org-babel-execute:git-permalink "ob-git-permalink" "\
Resolve BODY permalink and insert source code.
If PARAMS url is specified, the parameter is used.

\(fn BODY PARAMS)" nil nil)

(register-definition-prefixes "ob-git-permalink" '("ob-git-permalink-"))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; ob-git-permalink-autoloads.el ends here
